<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\ComponentDefinition;

use Capwelton\App\Article\Set\ArticleSet;
use Capwelton\App\Article\Ctrl\ArticleController;
use Capwelton\App\Article\Ui\ArticleUi;

use Capwelton\App\Article\Set\ArticleFamilySet;
use Capwelton\App\Article\Ctrl\ArticleFamilyController;
use Capwelton\App\Article\Ui\ArticleFamilyUi;

use Capwelton\App\Article\Set\ArticleUnitSet;
use Capwelton\App\Article\Ctrl\ArticleUnitController;
use Capwelton\App\Article\Ui\ArticleUnitUi;

use Capwelton\App\Article\Set\ArticleCategorySet;
use Capwelton\App\Article\Ctrl\ArticleCategoryController;
use Capwelton\App\Article\Ui\ArticleCategoryUi;

class ComponentDefinition implements \app_ComponentDefinition
{
    public function getDefinition()
    {
        return 'Manages articles';
    }
    
    public function getComponents(\Func_App $App)
    {
        $articleCategoryComponent = $App->createComponent(ArticleCategorySet::class, ArticleCategoryController::class, ArticleCategoryUi::class);
        $articleFamilyComponent = $App->createComponent(ArticleFamilySet::class, ArticleFamilyController::class, ArticleFamilyUi::class);
        $articleUnitComponent = $App->createComponent(ArticleUnitSet::class, ArticleUnitController::class, ArticleUnitUi::class);
        $articleComponent = $App->createComponent(ArticleSet::class, ArticleController::class, ArticleUi::class);
        
        return array(
            'ARTICLECATEGORY' => $articleCategoryComponent,
            'ARTICLEFAMILY' => $articleFamilyComponent,
            'ARTICLEUNIT' => $articleUnitComponent,
            'ARTICLE' => $articleComponent
        );
    }
    
    public function getLangPath(\Func_App $App)
    {
        $addon = $App->getAddon();
        if(!$addon){
            return null;
        }
        return $addon->getPhpPath().'vendor/capwelton/apparticle/src/langfiles/';
    }
    
    public function getStylePath(\Func_App $App)
    {
        $addon = $App->getAddon();
        if(!$addon){
            return null;
        }
        return $addon->getPhpPath().'vendor/capwelton/apparticle/src/styles/';
    }
    
    public function getScriptPath(\Func_App $App)
    {
        $addon = $App->getAddon();
        if(!$addon){
            return null;
        }
        return $addon->getPhpPath().'vendor/capwelton/apparticle/src/scripts/';
    }
    
    public function getConfiguration(\Func_App $App)
    {
        $W = bab_Widgets();
        $component = $App->getComponentByName('Article');
        return array(
            array(
                'sectionName' => 'Main',
                'sectionContent' => array(
                    $W->Link(
                        $W->Icon($component->translate('Article categories'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->ArticleCategory()->displayList()
                    ),
                    $W->Link(
                        $W->Icon($component->translate('Article families'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->ArticleFamily()->displayList()
                    ),
                    $W->Link(
                        $W->Icon($component->translate('Article units'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->ArticleUnit()->displayList()
                    )
                )
            )
        );
    }
}