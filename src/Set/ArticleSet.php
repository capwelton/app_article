<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */
namespace Capwelton\App\Article\Set;

use Capwelton\App\Article\Set\RecordViews\ArticleDefaultRecordView;

/**
 * A app_Address is any kind of postal address.
 *
 * @property ORM_TextField $recipient
 * @property ORM_TextField $street
 * @property ORM_StringField $postalCode
 * @property ORM_StringField $city
 * @property ORM_StringField $cityComplement
 * @property ORM_StringField $state
 * @property ORM_StringField $longitude
 * @property ORM_StringField $latitude
 * @property ORM_TextField $instructions
 * @property app_CountrySet $country
 *
 * @method app_Address get(mixed $criteria)
 * @method app_Address request(mixed $criteria)
 * @method app_Address[]|\ORM_Iterator select(\ORM_Criteria $criteria)
 * @method app_Address newRecord()
 *
 */
class ArticleSet extends \app_TraceableRecordSet
{
    /**
     *
     * @param
     *            Func_App App()
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        $this->setTableName($App->classPrefix.'Article');
        $App = $this->App();
        $this->setDescription('Article');
        
        $articleCmp = $App->getComponentByName('ARTICLE');
        
        $this->addFields(
            ORM_StringField('reference')->index()->setDescription($articleCmp->translate('Reference of the product')),
            ORM_StringField('name')->setDescription($articleCmp->translate('Name of the product')),
            ORM_StringField('subtitle')->setDescription($articleCmp->translate('Subtitle for the product name')),
            ORM_TextField('description')->setDescription($articleCmp->translate('Generic description field in HTML')),
            ORM_StringField('barcode', 255)->setDescription($articleCmp->translate('Barcode')),
            ORM_DecimalField('unitSellingPrice', 2)->setDescription($articleCmp->translate('Unit selling price')),
            ORM_DecimalField('quantity', 4)->setDescription($articleCmp->translate('Quantity')),
            ORM_BoolField('isSoldable')->setOutputOptions($articleCmp->translate('No'), $App->translate('Yes'))->setDescription($articleCmp->translate('Is soldable')),
            ORM_BoolField('isPurchasable')->setOutputOptions($articleCmp->translate('No'), $App->translate('Yes'))->setDescription($articleCmp->translate('Is purchasable')),
            ORM_BoolField('isStockable')->setOutputOptions($articleCmp->translate('No'), $App->translate('Yes'))->setDescription($articleCmp->translate('Is stockable')),
            ORM_BoolField('isRentable')->setOutputOptions($articleCmp->translate('No'), $App->translate('Yes'))->setDescription($articleCmp->translate('Is rentable')),
            ORM_BoolField('isProduceable')->setOutputOptions($articleCmp->translate('No'), $App->translate('Yes'))->setDescription($articleCmp->translate('Is produceable')),
            ORM_DecimalField('weight', 2)->setDescription($articleCmp->translate('Weight')),
            ORM_DecimalField('width', 2)->setDescription($articleCmp->translate('Width')),
            ORM_DecimalField('height', 2)->setDescription($articleCmp->translate('Height')),
            ORM_DecimalField('length', 2)->setDescription($articleCmp->translate('Length')),
            ORM_StringField('accountCode')->setDescription($articleCmp->translate('Account code')),
            ORM_StringField('sellingWarranty')->setDescription($articleCmp->translate('Selling warranty')),
            ORM_StringField('rentingAccountCode')->setDescription($articleCmp->translate('Renting account code'))
        );
        
        $this->hasOne('organization', $App->OrganizationSetClassName())->setDescription($articleCmp->translate('Organization'));
        $this->hasOne('category', $App->ArticleCategorySetClassName())->setDescription($articleCmp->translate('Category'));
        $this->hasOne('unit', $App->ArticleUnitSetClassName())->setDescription($articleCmp->translate('Unit'));
        $this->hasOne('stockingUnit', $App->ArticleUnitSetClassName())->setDescription($articleCmp->translate('Stocking unit'));
        $this->hasOne('rentingUnit', $App->ArticleUnitSetClassName())->setDescription($articleCmp->translate('Renting unit'));
        $this->hasOne('rentingCurrency', $App->CurrencySetClassName())->setDescription($articleCmp->translate('Renting currency'));
        $this->hasOne('family', $App->ArticleFamilySetClassName())->setDescription($articleCmp->translate('Article family'));
        $this->hasOne('sellingVatAccount', $App->VatAccountSetClassName())->setDescription($articleCmp->translate('Selling VAT account'));
        $this->hasOne('rentingVatAccount', $App->VatAccountSetClassName())->setDescription($articleCmp->translate('Renting VAT account'));
        
        foreach ($this->getCustomFields() as $customfield) {
            $this->addFields($customfield->getORMField());
        }
    }
    
    public function getRequiredComponents()
    {
        return array(
            'ORGANIZATION',
            'CURRENCY',
            'VATACCOUNT'
        );
    }
    
    public function getOptionalComponents()
    {
        return array(
            'ARTICLESTOCK'
        );
    }
    
    public function onUpdate()
    {
        $defaultView = new ArticleDefaultRecordView($this->App());
        $defaultView->generate();
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new ArticleBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new ArticleAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isCreatable()
    {
        return $this->isUpdatable();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}

class ArticleBeforeSaveEvent extends \RecordAfterSaveEvent
{
    
}

class ArticleAfterSaveEvent extends \RecordBeforeSaveEvent
{
    
}