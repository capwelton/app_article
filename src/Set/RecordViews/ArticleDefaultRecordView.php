<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Article\Set\RecordViews;

class ArticleDefaultRecordView
{
    /**
     * @var \Func_App
     */
    private $app;
    public function __construct(\Func_App $App)
    {
        $this->app = $App;
    }
    
    public function generate($force = false)
    {
        $App = $this->app;
        $customSectionSet = $App->CustomSectionSet();
        $object = $App->classPrefix.'Article';
        $view = '';
        
        $component = $App->getComponentByName('ARTICLE');
        
        $criteria = $customSectionSet->all(
            $customSectionSet->object->is($object)
            ->_AND_($customSectionSet->view->is($view))
        );
        
        $customSections = $customSectionSet->select(
            $criteria
        );
        
        if($customSections->count() > 0 && !$force)
        {
            return $this;
        }
        
        $customContainerSet = $App->CustomContainerSet();
        
        $containerCriteria = $customContainerSet->all(
            $customContainerSet->object->is($object)
            ->_AND_($customContainerSet->view->is($view))
        );
        
        $customContainerSet->delete($containerCriteria);
        $customSectionSet->delete($criteria);
        
        $mainContainer = $customContainerSet->newRecord();
        $mainContainer->view = $view;
        $mainContainer->object = $object;
        $mainContainer->sizePolicy = 'col-md-12';
        $mainContainer->layout = 'vbox';
        $mainContainer->name = 'main';
        $mainContainer->rank = 0;
        $mainContainer->save();
        
        $rank = 0;
        
        //Article
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-6';
        $customSection->fieldsLayout = 'horizontalLabel';
        $customSection->object = $object;
        $customSection->fields = '{"mainPhoto":{"block":"","fieldname":"mainPhoto","parameters":[]},"reference":{"block":"","fieldname":"reference","parameters":[]},"name":{"block":"","fieldname":"name","parameters":[]},"organization":{"block":"","fieldname":"organization","parameters":[]},"description":{"block":"","fieldname":"description","parameters":[]},"_fieldsGroup0":{"block":"","fieldname":"_fieldsGroup","parameters":[],"fields":{"isPurchasable":{"block":"","fieldname":"isPurchasable","parameters":[]},"isSoldable":{"block":"","fieldname":"isSoldable","parameters":[]},"isStockable":{"block":"","fieldname":"isStockable","parameters":[]}}},"_fieldsGroup1":{"block":"","fieldname":"_fieldsGroup","parameters":[],"fields":{"isRentable":{"block":"","fieldname":"isRentable","parameters":[]},"isProduceable":{"block":"","fieldname":"isProduceable","parameters":[]}}},"family":{"block":"","fieldname":"family","parameters":[]},"category":{"block":"","fieldname":"category","parameters":[]},"barcode":{"block":"","fieldname":"barcode","parameters":[]}}';
        $customSection->classname = 'box gradient magenta';
        $customSection->name = $component->translate('Article');
        $customSection->editable = true;
        $customSection->rank = $rank;
        $customSection->view = $view;
        $customSection->foldable = true;
        $customSection->container = $mainContainer->id;
        $customSection->save();
        
        $rank++;
        
        //Selling
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-6';
        $customSection->fieldsLayout = 'horizontalLabel';
        $customSection->object = $object;
        $customSection->fields = '{"unit":{"block":"","fieldname":"unit","parameters":[]},"currency":{"block":"","fieldname":"currency","parameters":[]},"sellingVatAccount":{"block":"","fieldname":"sellingVatAccount","parameters":[]},"unitSellingPrice":{"block":"","fieldname":"unitSellingPrice","parameters":[]},"accountCode":{"block":"","fieldname":"accountCode","parameters":[]},"sellingWarranty":{"block":"","fieldname":"sellingWarranty","parameters":[]}}';
        $customSection->classname = 'box gradient teal';
        $customSection->name = $component->translate('Selling');
        $customSection->editable = true;
        $customSection->rank = $rank;
        $customSection->view = $view;
        $customSection->foldable = true;
        $customSection->container = $mainContainer->id;
        $customSection->visibilityCriteria = '{ "isSoldable": { "is": "1" } }';
        $customSection->save();
        
        $rank ++;
        
        //Stock
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-6';
        $customSection->fieldsLayout = 'horizontalLabel';
        $customSection->object = $object;
        $customSection->fields = '{"stockingUnit":{"block":"","fieldname":"stockingUnit","parameters":[]},"dimensions":{"block":"","fieldname":"dimensions","parameters":[]},"weight":{"block":"","fieldname":"weight","parameters":[]}}';
        $customSection->classname = 'box gradient red';
        $customSection->name = $component->translate('Stock');
        $customSection->editable = true;
        $customSection->rank = $rank;
        $customSection->view = $view;
        $customSection->foldable = true;
        $customSection->container = $mainContainer->id;
        $customSection->visibilityCriteria = '{ "isStockable": { "is": "1" } }';
        $customSection->save();
        
        $rank ++;
        
        //Rent
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-6';
        $customSection->fieldsLayout = 'horizontalLabel';
        $customSection->object = $object;
        $customSection->fields = '{"_fieldsGroup0":{"block":"","fieldname":"_fieldsGroup","parameters":[],"fields":{"rentingVatAccount":{"block":"","fieldname":"rentingVatAccount","parameters":[]},"rentingCurrency":{"block":"","fieldname":"rentingCurrency","parameters":[]},"rentingUnit":{"block":"","fieldname":"rentingUnit","parameters":[]}}},"rentingAccountCode":{"block":"","fieldname":"rentingAccountCode","parameters":[]}}';
        $customSection->classname = 'box gradient blue';
        $customSection->name = $component->translate('Location');
        $customSection->editable = true;
        $customSection->rank = $rank;
        $customSection->view = $view;
        $customSection->foldable = true;
        $customSection->container = $mainContainer->id;
        $customSection->visibilityCriteria = '{ "isRentable": { "is": "1" } }';
        $customSection->save();
        
        $standardContainer = $customContainerSet->newRecord();
        $standardContainer->view = $view;
        $standardContainer->object = $object;
        $standardContainer->sizePolicy = 'col-md-12';
        $standardContainer->layout = 'vbox';
        $standardContainer->name = 'standard';
        $standardContainer->rank = 1;
        $standardContainer->save();
        
        $rank = 0;
        
        //Image gallery
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-12';
        $customSection->fieldsLayout = 'horizontalLabel';
        $customSection->object = $object;
        $customSection->fields = '{"gallery":{"block":"","fieldname":"gallery","parameters":[]}}';
        $customSection->classname = 'box';
        $customSection->name = $component->translate('Gallery');
        $customSection->editable = true;
        $customSection->rank = $rank;
        $customSection->view = $view;
        $customSection->foldable = true;
        $customSection->container = $standardContainer->id;
        $customSection->save();
        
        $rank++;
        
        //Nomenclature
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-12';
        $customSection->fieldsLayout = 'horizontalLabel';
        $customSection->object = $object;
        $customSection->fields = '';
        $customSection->classname = 'box gradient yellow';
        $customSection->name = $component->translate('Nomenclature');
        $customSection->editable = true;
        $customSection->rank = $rank;
        $customSection->view = $view;
        $customSection->foldable = true;
        $customSection->container = $standardContainer->id;
        $customSection->save();
        
        $rank++;
        
        //Variants
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-12';
        $customSection->fieldsLayout = 'horizontalLabel';
        $customSection->object = $object;
        $customSection->fields = '';
        $customSection->classname = 'box gradient pink';
        $customSection->name = $component->translate('Variants');
        $customSection->editable = true;
        $customSection->rank = $rank;
        $customSection->view = $view;
        $customSection->foldable = true;
        $customSection->container = $standardContainer->id;
        $customSection->save();
        
        $rank++;
        
        if($taskComponent = $App->getComponentByName('Task')){
            //Tasks
            $customSection = $customSectionSet->newRecord();
            $customSection->sizePolicy = 'col-md-6';
            $customSection->fieldsLayout = 'verticalLabel';
            $customSection->object = $object;
            $customSection->fields = 'pendingTasks:type=;label=__;classname=;sizePolicy=';
            $customSection->classname = 'box blue gradient';
            $customSection->editable = false;
            $customSection->name = $taskComponent->translate('Actions/Tasks');
            $customSection->rank = $rank++;
            $customSection->view = $view;
            $customSection->container = $standardContainer->id;
            $customSection->save();
        }
        
        if($noteComponent = $App->getComponentByName('Note')){
            //Notes
            $customSection = $customSectionSet->newRecord();
            $customSection->sizePolicy = 'col-md-6';
            $customSection->fieldsLayout = 'noLabel';
            $customSection->object = $object;
            $customSection->fields = 'notes';
            $customSection->classname = 'box green gradient';
            $customSection->editable = false;
            $customSection->name = $noteComponent->translate('Notes');
            $customSection->rank = $rank++;
            $customSection->view = $view;
            $customSection->container = $standardContainer->id;
            $customSection->save();
        }
    }
}