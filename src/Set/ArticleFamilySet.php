<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Article\Set;

include_once 'base.php';

/**
 * @method ArticleFamily[]  select()
 * @method ArticleFamily    get()
 * @method ArticleFamily    newRecord()
 * @method ArticleFamilySet lf()
 * @method ArticleFamilySet lr()
 * @method ArticleFamilySet parent()
 * @method Func_App                 App()
 *
 * @property \ORM_StringField   $name
 * @property ArticleFamilySet   $lf
 * @property ArticleFamilySet   $lr
 * @property ArticleFamilySet   $parent
 */
class ArticleFamilySet extends \app_TraceableRecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'ArticleFamily');
        $this->setPrimaryKey('id');
        $this->setDescription('ArticleFamily');
        
        $this->addFields(
            ORM_StringField('name')->setDescription($App->translatable('Name'))
            );
        
        $ArticleFamilySetClassName = $App->ArticleFamilySetClassName();
        
        $this->hasOne('lf', $ArticleFamilySetClassName);
        $this->hasOne('lr', $ArticleFamilySetClassName);
        $this->hasOne('parent', $ArticleFamilySetClassName);
        
        $this->addCustomFields();
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new ArticleFamilyBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new ArticleFamilyAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    public function onUpdate()
    {
        $this->instanciateFirstFamily();
    }
    
    private function instanciateFirstFamily()
    {
        $App = $this->App();
        $set = $App->ArticleFamilySet();
        if ($set->select()->count() == 0) {
            // We must have a root node with id = 1
            $status = $set->newRecord();
            $status->id = 1;
            $status->parent = 0;
            $status->save();
            
            $first = $set->newRecord();
            $first->id = 2;
            $first->parent = $status->id;
            $first->name = $App->translate('Miscellaneous');
            $first->save();
        }
    }
    
    /**
     *
     * @return\ ORM_Criterion
     */
    public function isChildOf($node)
    {
        if (!is_int($node)) {
            $node = $node->id;
        }
        return $this->id_parent->is($node);
    }
    
    /**
     *
     * @return \ORM_Criterion
     */
    public function isDescendantOf($node)
    {
        if (is_numeric($node)) {
            $set = new self();
            $node = $set->get($node);
        }
        return $this->lf->greaterThan($node->lf)->_AND_($this->lr->lessThan($node->lr));
    }
    
    /**
     * Updates lf and lr on tree nodes.
     *
     * @internal
     */
    public function update($lr, $offset = 1)
    {
        global $babDB;
        
        $offset *= 2;
        $operation = ($offset > 0 ? '+' : '-');
        $offset = abs($offset);
        $babDB->db_query('UPDATE ' . $this->getTableName() . ' SET lr = lr' . $operation . $offset . ' WHERE lr > ' . $lr);
        $babDB->db_query('UPDATE ' . $this->getTableName() . ' SET lf = lf' . $operation . $offset . ' WHERE lf > ' . $lr);
    }
    
    /**
     * Delete records, the number of records depends on criteria
     *
     * @param \ORM_Criteria	$oCriteria	Criteria for selecting records to delete
     *
     * @return boolean					True on success, False otherwise
     */
    public function delete(\ORM_Criteria $criteria = null)
    {
        $nodes = $this->select($criteria);
        foreach ($nodes as $node) {
            
            $lf = $node->lf;
            $lr = $node->lr;
            
            if ($lr - $lf > 1) {
                $subTreeCriteria = $this->lf->greaterThanOrEqual($lf)->_AND_($this->lf->lessThanOrEqual($lr));
                parent::delete($subTreeCriteria);
                $this->update($lr, ($lr - $lf + 1) / 2);
            } else {
                parent::delete($this->id->is($node->id));
            }
        }
        return true;
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isCreatable()
    {
        return $this->isUpdatable();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}

class ArticleFamilyBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class ArticleFamilyAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}