<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */
namespace Capwelton\App\Article\Set;

/**
 * @property \ORM_StringField   $name
 * @property \ORM_TextField     $description
 * 
 * @method \Func_App    App()
 * @method ArticleUnit  get(mixed $criteria)
 * @method ArticleUnit  request(mixed $criteria)
 * @method ArticleUnit[]|\ORM_Iterator select(\ORM_Criteria $criteria)
 * @method ArticleUnit  newRecord()
 */
class ArticleUnitSet extends \app_TraceableRecordSet
{
    /**
     *
     * @param Func_App App()
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        $this->setTableName($App->classPrefix.'ArticleUnit');
        $App = $this->App();
        $this->setDescription('ArticleUnit');
        
        $this->addFields(
            ORM_StringField('name')->setDescription($App->translate('Name')),
            ORM_TextField('description')->setDescription($App->translate('Description'))
        );
        
        foreach ($this->getCustomFields() as $customfield) {
            $this->addFields($customfield->getORMField());
        }
    }
    
    public function onUpdate()
    {
        $App = $this->App();
        $set = $App->ArticleUnitSet();
        if($set->select()->count() == 0){
            $defaultUnit = $set->newRecord();
            $defaultUnit->name = $App->translate('Unit');
            $defaultUnit->save();
        }
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new ArticleUnitBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new ArticleUnitAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isCreatable()
    {
        return $this->isUpdatable();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }
    
    /**
     *
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}

class ArticleUnitBeforeSaveEvent extends \RecordAfterSaveEvent
{
    
}

class ArticleUnitAfterSaveEvent extends \RecordBeforeSaveEvent
{
    
}