<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ctrl;

use Capwelton\App\Article\Set\ArticleUnitSet;
use Capwelton\App\Article\Set\ArticleUnit;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('ArticleUnit');

/**
 * This controller manages actions that can be performed on article units.
 *
 * @method \Func_App    App()
 */
class ArticleUnitController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('ArticleUnit'));
    }
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param ArticleUnitSet   $teamTypeSet
     * @param array         $filter
     * @return \ORM_Criteria
     */
    protected function getFilterCriteria(ArticleUnitSet $set, $filter)
    {
        // Initial conditions are base on read access rights.
        $conditions = new \ORM_TrueCriterion();
        
        if (isset($filter['name']) && !empty($filter['name'])) {
            $conditions = $conditions->_AND_(
                $set->name->contains($filter['name'])
            );
        }
        
        return $conditions;
    }
    
    protected function toolbar($tableView)
    {
        $toolbar = parent::toolbar($tableView);
        $W = bab_Widgets();
        $toolbar->addItem(
            $W->Link(
                $this->App()->translate('Create a new article unit'),
                $this->proxy()->edit()
            )->addClass('icon', \Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        return $toolbar;
    }
    
    /**
     * Editor article unit form
     *
     * @param int | null $id
     * @return \app_Page
     */
    public function edit($id = null, $errors = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        $page = $Ui->Page();
        
        $set = $App->ArticleUnitSet();
        
        $page->addClass('app-page-editor');
        
        if (isset($id)) {
            $page->setTitle($App->translate('Edit article unit'));
            $id = $set->request($id);
        } else {
            $page->setTitle($App->translate('Create a new article unit'));
        }
        
        $editor = $Ui->ArticleUnitEditor($id);
        $editor->isAjax = bab_isAjaxRequest();
        $editor->setName('data');
        
        $page->addItem($editor);
        
        return $page;
    }
    
    /**
     * @param array $data
     * @return boolean
     */
    public function save($data = null)
    {
        $App = $this->App();
        
        $set = $App->ArticleUnitSet();
        
        if (isset($data['id'])) {
            $record = $set->request($data['id']);
        } else {
            $record = $set->newRecord();
        }
        
        $record->setFormInputValues($data);
        
        $record->save();
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-ArticleUnitController_modelView');
        
        return true;
    }
    
    
    
    public function delete($id)
    {
        $this->requireDeleteMethod();
        $App = $this->App();
        
        $set = $App->ArticleUnitSet();
        
        $record = $set->request($id);
        
        $set->delete($set->id->is($record->id));
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-ArticleUnitController_modelView');
        
        return true;
    }
    
    /**
     * Does nothing and returns to the previous page.
     * @return bool
     */
    public function cancel()
    {
        return true;
    }
}