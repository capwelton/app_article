<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ctrl;

use Capwelton\App\Article\Set\ArticleFamilySet;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('ArticleFamily');

/**
 * This controller manages actions that can be performed on article families.
 *
 * @method \Func_App    App()
 */
class ArticleFamilyController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('ArticleFamily'));
    }
    
    /**
     * @return \app_Page
     */
    public function displayList()
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        $page->addItem($W->Title($App->translate('Article families'), 1));
        
        $Ui = $App->Ui();
        
        $page->addItem($Ui->ArticleFamilyList());
        $page->setReloadAction($this->proxy()->displayList());
        $page->addClass('depends-articleFamilyList');
        
        return $page;
    }
    
    /**
     * @return \app_Page
     */
    public function add($parentFamily)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $set = $App->ArticleFamilySet();
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        $page->addItem($W->Title($App->translate('Add family'), 1));
        
        $Ui = $App->Ui();
        
        $editor = $Ui->ArticleFamilyEditor();
        $editor->setName('data');
        
        $parentFamily = $set->get($parentFamily);
        if (isset($parentFamily->id)) {
            $editor->addItem(
                $W->Hidden(null, 'parent', $parentFamily->id)
            );
        }
        
        $editor->setSaveAction($this->proxy()->save());
        $editor->setCancelAction($this->proxy()->cancel());
        
        $page->addItem($editor);
        
        return $page;
    }
    
    /**
     * @return \app_Page
     */
    public function edit($id = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $set = $App->ArticleFamilySet();
        $id = $set->get($id);
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        $page->addItem($W->Title($App->translate('Edit family'), 1));
        
        $Ui = $App->Ui();
        
        $classificationEditor = $Ui->ArticleFamilyEditor();
        $classificationEditor->setName('data');
        if (isset($id)) {
            $classificationEditor->addItem(
                $W->Hidden(null, 'id', $id->id)    
            );
            $classificationEditor->setRecord($id);
        }
        
        $classificationEditor->setSaveAction($this->proxy()->save());
        $classificationEditor->setCancelAction($this->proxy()->cancel());
        
        $page->addItem($classificationEditor);
        
        return $page;
    }
    
    /**
     * @param array $data
     * @return boolean
     */
    public function save($data = null)
    {
        $App = $this->App();
        
        $set = $App->ArticleFamilySet();
        
        if (isset($data['id'])) {
            $record = $set->request($data['id']);
            $record->setFormInputValues($data);
            $record->save();
        } else {
            $record = $set->newRecord();
            $record->setFormInputValues($data);
            $parent = $set->get($data['parent']);
            $parent->appendChild($record);
        }
        
        $this->addMessage($App->translate('The article family has been saved'));        
        app_redirect($this->proxy()->displayList());
        
        return true;
    }
    
    /**
     * Displays a page asking to confirm the deletion of a record.
     *
     * @param int $id
     */
    public function confirmDelete($id)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        
        $recordSet = $this->getRecordSet();
        $record = $recordSet->get($id);
        
        $page = $Ui->Page();
        
        $page->addClass('app-page-editor');
        $page->setTitle($App->translate('Deletion'));
        
        if (!isset($record)) {
            $page->addItem(
                $W->Label($App->translate('This element does not exists'))
            );
            $page->addClass('alert', 'alert-warning');
            return $page;
        }
        
        
        $form = new \app_Editor($App);
        
        $form->addItem($W->Hidden(null, 'id', $id));
        
        $recordTitle = $record->getRecordTitle();
        
        $subTitle = $App->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));
        
        $form->addItem($W->Title($App->translate('Confirm delete?'), 6));
        
        $form->addItem($W->Html(bab_toHtml($App->translate('It will not be possible to undo this deletion'))));
        
        $confirmedAction = $this->proxy()->delete($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAction($confirmedAction)
            ->setLabel($App->translate('Delete'))
        );
        $form->addButton($W->SubmitButton()->setLabel($App->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);
        
        return $page;
    }
    
    public function delete($id)
    {
        $this->requireDeleteMethod();
        $App = $this->App();
        
        $set = $App->ArticleFamilySet();
        $record = $set->request($set->id->is($id));
        $set->delete($set->id->is($record->id));
        
        $this->addMessage($App->translate('The article family has been deleted')); 
        app_redirect($this->proxy()->displayList());
        
        return true;
    }
    
    /**
     * Does nothing and returns to the previous page.
     * @return bool
     */
    public function cancel()
    {
        return true;
    }
}