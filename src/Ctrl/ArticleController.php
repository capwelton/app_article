<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ctrl;

use Capwelton\App\Article\Set\ArticleSet;
use Capwelton\App\Article\Set\Article;
use Capwelton\App\Article\Ui\ArticleViewEditor;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('Article');

/**
 * This controller manages actions that can be performed on article types.
 *
 * @method \Func_App    App()
 */
class ArticleController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('Article'));
    }
    
    public function getAvailableDisplayFields()
    {
        $App = $this->App();
        $articleCmp = $App->getComponentByName('ARTICLE');
        $currencyCmp = $App->getComponentByName('CURRENCY');
        $availableFields = array(
            'mainPhoto' => array(
                'name' => 'mainPhoto',
                'description' => $articleCmp->translate('Main photo')
            ),
            'gallery' => array(
                'name' => 'gallery',
                'description' => $articleCmp->translate('Gallery')
            ),
            'currency' => array(
                'name' => 'currency',
                'description' => $currencyCmp->translate('Currency')
            ),
            'sellingVatAccount' => array(
                'name' => 'sellingVatAccount',
                'description' => $articleCmp->translate('Selling VAT account')
            ),
            'dimensions' => array(
                'name' => 'dimensions',
                'description' => $articleCmp->translate('Dimensions')
            ),
            'rentingVatAccount' => array(
                'name' => 'rentingVatAccount',
                'description' => $articleCmp->translate('Renting VAT account')
            ),
            'rentingCurrency' => array(
                'name' => 'rentingCurrency',
                'description' => $currencyCmp->translate('Renting currency')
            )
        );
        
        if($attachmentC = $App->getComponentByName('ATTACHMENT')){
            $availableFields['attachments'] = array(
                'name' => 'attachments',
                'description' => $attachmentC->translate('Attached files')
            );
        }
        if($tagC = $App->getComponentByName('TAG')){
            $availableFields['tags'] =  array(
                'name' => 'tags',
                'description' => $tagC->translate('Tags')
            );
        }
        if($noteC = $App->getComponentByName('NOTE')){
            $availableFields['notes'] = array(
                'name' => 'notes',
                'description' => $noteC->translate('Notes')
            );
        }
        if($teamC = $App->getComponentByName('TEAM')){
            $availableFields['teams'] = array(
                'name' => 'teams',
                'description' => $teamC->translate('Teams')
            );
        }
        if($taskC = $App->getComponentByName('TASK')){
            $availableFields['pendingTasks'] = array(
                'name' => 'pendingTasks',
                'description' => $taskC->translate('Pending tasks')
            );
            $availableFields['completedTasks'] = array(
                'name' => 'completedTasks',
                'description' => $taskC->translate('Completed tasks')
            );
        }
        
        $availableFields = array_merge($availableFields, parent::getAvailableDisplayFields());
        
        return $availableFields;
    }
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param ArticleSet   $teamTypeSet
     * @param array         $filter
     * @return \ORM_Criteria
     */
    protected function getFilterCriteria(ArticleSet $set, $filter)
    {
        // Initial conditions are base on read access rights.
        $conditions = new \ORM_TrueCriterion();
        
        if (isset($filter['name']) && !empty($filter['name'])) {
            $conditions = $conditions->_AND_(
                $set->name->contains($filter['name'])
            );
        }
        
        if (isset($filter['reference']) && !empty($filter['reference'])) {
            $conditions = $conditions->_AND_(
                $set->reference->contains($filter['reference'])
            );
        }
        
        return $conditions;
    }
    
    protected function toolbar($tableView)
    {
        $toolbar = parent::toolbar($tableView);
        $W = bab_Widgets();
        $toolbar->addItem(
            $W->Link(
                $this->App()->translate('Create a new article'),
                $this->proxy()->edit()
            )->addClass('icon', \Func_Icons::ACTIONS_LIST_ADD)
        );
        return $toolbar;
    }
    
    
    /**
     * @param Article $record
     * @return array
     */
    protected function getActions(Article $record)
    {
        $App = $this->App();
        
        $ctrl = $this->proxy();
        $actions = array();
        $moreActions = array();
        $documentsActions = array();
        
        if ($record->isReadable()) {
            $action = $App->Controller()->Attachment()->attachments($record->getRef());
            $action->setTitle($App->translate('Attached files'));
            $action->setIcon(\Func_Icons::APPS_FILE_MANAGER);
            $documentsActions['attachedFiles'] = $action;
        }
        
        
        if ($record->isDeletable()) {
            $action = $ctrl->confirmDelete($record->id);
            $action->setTitle($App->translate('Delete'));
            $action->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE);
            $moreActions['delete'] = $action;
        }
        
        if (bab_isUserAdministrator()) {
            $customContainerCtrl = $App->Controller()->CustomContainer();
            $views = $record->getViews();
            $className = $record->getClassName();
            
            foreach ($views as $view) {
                $action = $customContainerCtrl->editContainers($className, $view);
                $action->setTitle(sprintf($App->translate('Edit view layout %s'), $view));
                $action->setIcon(\Func_Icons::ACTIONS_VIEW_PIM_JOURNAL);
                $moreActions['editSections_' . $view] = $action;
            }
            
            $action = $customContainerCtrl->importContainersConfirm($className, $view);
            $action->setTitle($App->translate('Import view layout'));
            $action->setIcon(\Func_Icons::ACTIONS_VIEW_PIM_JOURNAL);
            $moreActions['importSectionsConfirm'] = $action;
        }
        
        $actions['documents'] = array(
            'icon' => \Func_Icons::APPS_FILE_MANAGER,
            'title' => 'Documents',
            'items' => array(
                $documentsActions
            )
        );
        $actions['more'] = array(
            'icon' => 'actions-context-menu'/*Func_Icons::ACTIONS_CONTEXT_MENU*/,
            'title' => 'More',
            'items' => array(
                $moreActions
            )
        );
        return $actions;
    }
    
    /**
     * Editor article form
     *
     * @param int | null $id
     * @return \app_Page
     */
    public function edit($id = null, $errors = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        $page = $Ui->Page();
        
        $set = $App->ArticleSet();
        
        $page->addClass('app-page-editor');
        
        /* @var $record Article */
        if (isset($id)) {
            $page->setTitle($App->translate('Edit article'));
            $record = $set->request($id);
        } else {
            $page->setTitle($App->translate('Create a new article'));
            $record = $set->newRecord();
            $record->deleted = \app_TraceableRecord::DELETED_STATUS_DRAFT;
            $record->isSoldable = true;
            
            $vatSet = $App->VatAccountSet();
            $defaultVatAccount = $vatSet->get($vatSet->isDefault->is(true));
            if($defaultVatAccount){
                $record->sellingVatAccount = $defaultVatAccount->id;
            }
            
            $record->save();
        }
        
        $page->addItem($this->creationEditor($record->id));
        
        return $page;
    }
    
    public function creationEditor($articleId, $view = '', $itemId = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $set = $App->ArticleSet();
        $set->setDefaultCriteria($set->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT)));
        
        $record = $set->request($set->id->is($articleId));
        $draft = $record->deleted == \app_TraceableRecord::DELETED_STATUS_DRAFT;
        
        /* @var $editor ArticleViewEditor */
        $editor = $Ui->ArticleViewEditor($record, '', $itemId);
        $editor->addClass('widget-no-close');
        $editor->setValues($record->getFormOutputValues());
        $editor->isAjax = bab_isAjaxRequest();
        $editor->setReloadAction($this->proxy()->creationEditor($record->id, $view, $editor->getId()));
        $editor->addClass('depends-ArticleCreation');
        
        if($draft){
            $editor->setAjaxAction($this->proxy()->saveDraft(), '', 'change');
        }
        
        if(!isset($itemId)){
            $editor->setSaveAction($this->proxy()->save());
        }
        
        return $editor;
    }
    
    /**
     * @param array $data
     * @return boolean
     */
    public function save($data = null)
    {
        $this->requireSaveMethod();
        $App = $this->App();
        
        $set = $App->ArticleSet();
        $set->setDefaultCriteria($set->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT)));
        
        if (isset($data['id'])) {
            $record = $set->request($data['id']);
        } else {
            $record = $set->newRecord();
        }
        
        $record->setFormInputValues($data);
        
        if($record->save()){
            $this->postSave($record, $data);
        }
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-ArticleController_modelView');
        
        return true;
    }    
    
    public function saveDraft($data = null)
    {
        $this->requireSaveMethod();
        
        $App = $this->App();
        $set = $App->ArticleSet();
        $set->setDefaultCriteria($set->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT)));
        
        $record = $set->request($set->id->is($data['id']));
        
        $isSoldable = $record->isSoldable;
        $isPurchasable = $record->isPurchasable;
        $isStockable = $record->isStockable;
        $isRentable = $record->isRentable;
        
        $record->setFormInputValues($data);
        $record->save();
        if($isSoldable != $record->isSoldable || $isPurchasable != $record->isPurchasable || $isStockable != $record->isStockable || $isRentable != $record->isRentable){
            $this->addReloadSelector('.depends-ArticleCreation');
        }
        
        return true;
    }
    
    protected function postSave(Article $record, $data)
    {
        parent::postSave($record, $data);
        if(isset($data['articleFamilies'])){
            $App = $this->App();
            $articleFamilySet = $App->ArticleFamilySet();
            $record->family = 0;
            foreach ($data['articleFamilies'] as $articleFamilyId => $dummy) {
                $articleFamily = $articleFamilySet->get($articleFamilyId);
                if (isset($articleFamily)) {
                    $record->family = $articleFamily->id;
                }
                break;
            }
            $record->save();
        }
        if($record->deleted == \app_TraceableRecord::DELETED_STATUS_DRAFT){
            $record->deleted = \app_TraceableRecord::DELETED_STATUS_EXISTING;
            $record->save();
            $this->redirect($this->proxy()->display($record->id));
        }
    }
    
    
    public function delete($id)
    {
        $this->requireDeleteMethod();
        $App = $this->App();
        
        $set = $App->ArticleSet();
        
        $record = $set->request($id);
        
        $set->delete($set->id->is($record->id));
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        $this->addReloadSelector('.depends-ArticleController_modelView');
        
        return true;
    }
    
    /**
     * Does nothing and returns to the previous page.
     * @return bool
     */
    public function cancel()
    {
        return true;
    }
    
    public function editFamily($id = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();
        
        $articleSet = $this->getRecordSet();
        $record = $articleSet->request($id);
        
        $page = $Ui->Page();
        $page->addClass('app-page-editor');
        
        $page->setTitle($App->translate('Edit article family'));
        
        $editor = $Ui->ArticleFamilyArticleEditor($record);
        $editor->isAjax = true;
        
        $editor->setName('data');
        $editor->addItem($W->Hidden(null, 'id', $record->id));
        
        $editor->setCancelAction($this->proxy()->cancel());
        $editor->setSaveAction($this->proxy()->saveFamily(), $App->translate('Save the article family'));
        
        $page->addItem($editor);
        
        return $page;
    }
    
    public function saveFamily($data = null)
    {
        $this->requireSaveMethod();
        
        $App = $this->App();
        
        $articleSet = $this->getRecordSet();
        $article = $articleSet->request($data['id']);
        $articleFamilySet = $App->ArticleFamilySet();
        $article->family = 0;
        foreach ($data['articleFamilies'] as $articleFamilyId => $dummy) {
            $articleFamily = $articleFamilySet->get($articleFamilyId);
            if (isset($articleFamily)) {
                $article->family = $articleFamily->id;
            }
            break;
        }
        
        $article->save();
        return true;
    }
    
    /**
     * Search in all articles based on the name and reference
     *
     * @param string $q
     * @return string (json)
     */
    public function search($q = null){
        $App = $this->App();
        $set = $this->getRecordSet();
        $W = bab_Widgets();
        $W->includePhpClass('Widget_Select2');
        $collection = new \Widget_Select2OptionsCollection();
        
        if(strlen($q)<3){
            $collection->addOption(new \Widget_Select2Option(0, $App->translate("The research is too short")));
            header("Content-type: application/json; charset=utf-8");
            echo $collection->output();
            die();
        }
        $records = $set->select(
            $set->any(
                array(
                    $set->name->contains($q),
                    $set->reference->contains($q)
                )
            )
        )->orderAsc($set->name);
            
        $counter = 0;
        foreach($records as $record){
            $counter++;
            $collection->addOption(
                new \Widget_Select2Option($record->id, $record->name, $record->fullNumber)
            );
        }
        
        header("Content-type: application/json; charset=utf-8");
        echo $collection->output();
        die();
    }
    
    public function reloadPhotos($class)
    {
        $this->addReloadSelector($class);
        return true;
    }
    
    public function _photos($articleId, $editable = false, $onlyMainPhoto = true, $width = 300, $height = 300, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        /* @var $record Article */
        $recordSet = $this->getRecordSet();
        $recordSet->setDefaultCriteria($recordSet->deleted->in(array(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT)));
        $record = $recordSet->request($articleId);

        $class = 'depends-articlePhotos';
        $class .= !$onlyMainPhoto ? 'photo-gallery' : 'main-photo';
        $box = $W->VBoxLayout($itemId);
        $box->setReloadAction(
            $this->proxy()->_photos($record->id, $editable, $onlyMainPhoto, $width, $height, $box->getId())
        );

        if (!$onlyMainPhoto) {
            $photoGalleryPaths = array();
            $filePickerIterator = $record->getOthersPhotosIterator();
            $mainCarousel = $W->Carousel()->addClass('app_article_carousel');
            $navCarousel = $W->Carousel()->addClass('app_article_carousel_nav');
            
            $mainCarousel->setOption('slidesToShow', 1);
            $mainCarousel->setOption('infinite', false);
            $mainCarousel->setOption('arrows', false);
            $mainCarousel->setOption('dots', false);
            $mainCarousel->setOption('centerMode', true);
            $mainCarousel->setOption('adaptiveHeight', true);
            $mainCarousel->setOption('asNavFor', '.app_article_carousel_nav');
            
            $navCarousel->setOption('adaptiveHeight', true);
            $navCarousel->setOption('variableWidth', true);
            $navCarousel->setOption('focusOnSelect', true);
            $navCarousel->setOption('asNavFor', '.app_article_carousel');
            
            $mainCarouselId = $mainCarousel->getId();
            $index = 1;
            foreach( $filePickerIterator as $file) {
                $smallPhotoUrl = app_getResizedImageUrl($file->getFilePath());
                $bigPhotoUrl = app_getResizedImageUrl($file->getFilePath(), 1000, 1000);
                $mainCarousel->addItem(
                    $W->FlowItems(
                        $W->ImageZoomer($smallPhotoUrl, $bigPhotoUrl)->setImageSet($mainCarouselId.'_'.$index)
                    )
                );
                $navCarousel->addItem(
                    $W->Image(app_getResizedImageUrl($file->getFilePath(), 150, 150))
                );
                $index++;
            }
            foreach( $filePickerIterator as $file) {
                $smallPhotoUrl = app_getResizedImageUrl($file->getFilePath());
                $bigPhotoUrl = app_getResizedImageUrl($file->getFilePath(), 1000, 1000);
                $mainCarousel->addItem(
                    $W->FlowItems(
                        $W->ImageZoomer($smallPhotoUrl, $bigPhotoUrl)->setImageSet($mainCarouselId.'_'.$index)
                    )
                );
                $navCarousel->addItem(
                    $W->Image(app_getResizedImageUrl($file->getFilePath(), 150, 150))
                );
                $index++;
            }
            $box->addItem($photoItem = $W->Image());
            $box->addItem($mainCarousel);
            $box->addItem($navCarousel);
        } else {
            $photoPath = $record->getPhotoPath();
            $photoUrl = app_getResizedImageUrl($photoPath);
            $photoItem = $W->Image();
            if($photoUrl){
                $photoItem->setImageUrl($photoUrl);
                $photoItem->addClass('img-responsive');
            }
            $box->addItem($photoItem);
        }

        if (isset($photoItem) && $editable && $record->isUpdatable()) {
            $imagesFormItem = $W->ImagePicker();
            $imagesFormItem->hideFiles();
            $imagesFormItem->setAjaxAction(
                $this->proxy()->reloadPhotos($class)
            );

            if (isset($photoItem)) {
                $imagesFormItem->setAssociatedDropTarget($photoItem);
            }

            if (!$onlyMainPhoto) {
                $photoUploadPath = $record->getOthersPhotosUploadPath();
            } else {
                $imagesFormItem->oneFileMode();
                $photoUploadPath = $record->getPhotoUploadPath();
            }

            if (!is_dir($photoUploadPath->toString())) {
                $photoUploadPath->createDir();
            }
            $imagesFormItem->setFolder($photoUploadPath);

            $box->addItem(
                $imagesFormItem
            );
            $box->addClass('app-image-upload');
        }

        return $box;
    }
    
    public function displayPhoto($articleId, $fileName)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        
        /* @var $record Article */
        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($articleId);
        
        $filePickerIterator = $record->getOthersPhotosIterator();
        foreach( $filePickerIterator as $file) {
            if($file->getFileName() == $fileName){
                $file->download();
            }
        }
    }
}