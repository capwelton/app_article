<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ui;

class ArticleFamilyList extends \Widget_Widget
{
    private $App;
    
    public function __construct(\Func_App $App, $id = null)
    {
        parent::__construct($id);
        $this->App = $App;
    }
    
    public function App()
    {
        return $this->App;
    }
    
    public function familyList()
    {        
        $App = $this->App();
        $W = bab_Widgets();
        
        $treeview = $W->SimpleTreeView();
        $treeview->setDefaultState(\Widget_SimpleTreeView::DEFAULT_STATE_EXPANDED);
        $treeview->addClass(\Func_Icons::ICON_LEFT_16);
        
        $set = $App->ArticleFamilySet();
        $nodes = $set->select($set->parent->greaterThan('0'));
        
        $ClassificationCtrl = $App->Controller()->ArticleFamily();
        
        $element = $treeview->createElement('1', '', '');
        $element->setItem($W->Label($App->translate('Families')));
        
        $treeview->appendElement($element, null);
        $element->addAction(
            'appendChild',
            $App->translate('Add subfamily'),
            $GLOBALS['babSkinPath'] . 'images/Puces/edit_add.png',
            $ClassificationCtrl->add('1')->url(),
            ''
        );
        
        foreach ($nodes as $node) {
            $element = $treeview->createElement(
                $node->id,
                '',
                $node->name,
                '',
                $ClassificationCtrl->edit($node->id)->url()
            );
            $element->setItem(
                $W->FlowItems(
                    $W->Frame()->setCanvasOptions(
                        \Widget_Item::Options()->backgroundColor('#' . $node->color)
                    )->addClass('app-color-preview'),
                    $W->Icon($node->name, $node->icon)
                )->setVerticalAlign('middle')
                ->setHorizontalSpacing(8, 'px')
            );
            
            $element->addAction(
                'appendChild',
                $App->translate('Add subfamily'),
                $GLOBALS['babSkinPath'] . 'images/Puces/edit_add.png',
                $ClassificationCtrl->add($node->id)->url(),
                ''
            );
            
            $ClassificationCanBeDeleted = true;
            if ($ClassificationCanBeDeleted /* || bab_isUserAdministrator() */ ) {
                $element->addAction(
                    'delete',
                    $App->translate('Delete'),
                    $GLOBALS['babSkinPath'] . 'images/Puces/delete.png',
                    $ClassificationCtrl->confirmDelete($node->id)->url(),
                    ''
                );
            }
            $parentId = $node->parent;
            $treeview->appendElement($element, $parentId);
        }
        $treeview->setPersistent(true);
        $treeview->setReloadPersistent(true);
        return $treeview;
    }
    
    public function display(\Widget_Canvas $canvas)
    {
        $output = parent::display($canvas);        
        $output .= $canvas->html($this->getId(), $this->getClasses(), $this->familyList());
        
        return $output;
    }
}