<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ui;

use Capwelton\App\Article\Ctrl\ArticleController;
use Capwelton\App\Article\Set\ArticleSet;
use Capwelton\App\Article\Set\Article;

class ArticleTableView extends \app_TableModelView
{
    /**
     * @var ArticleController
     */
    protected $ctrl;
    
    public function __construct(\Func_App $app = null, $id = null)
    {
        $this->ctrl = $app->Controller()->Article();
        parent::__construct($app, $id);
    }
    
    public function addDefaultColumns(ArticleSet $set = null)
    {
        if (!isset($set)) {
            $set = $this->getRecordSet();
        }
        $set->family();
        $set->category();
        $set->unit();
        $set->organization()->currency();
        $set->sellingVatAccount();
        $App = $this->App();
        $cmp = $App->getComponentByName('ARTICLE');
        $this->addColumn(widget_TableModelViewColumn('visual', $cmp->translate('Visual'))->addClass('app-article-visual-column'));
        $this->addColumn(app_TableModelViewColumn($set->reference)->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
        $this->addColumn(app_TableModelViewColumn($set->name)->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
        $this->addColumn(app_TableModelViewColumn($set->family->name, $cmp->translate('Family'))->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
        $this->addColumn(app_TableModelViewColumn($set->category->name, $cmp->translate('Category'))->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
        $this->addColumn(app_TableModelViewColumn($set->isPurchasable)->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
        $this->addColumn(app_TableModelViewColumn($set->isSoldable)->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
        $this->addColumn(app_TableModelViewColumn($set->isStockable)->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
        $this->addColumn(app_TableModelViewColumn($set->isRentable)->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
        $this->addColumn(app_TableModelViewColumn($set->unitSellingPrice)->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
        $this->addColumn(app_TableModelViewColumn($set->organization->currency->name_fr, $cmp->translate('Currency'))->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
        $this->addColumn(app_TableModelViewColumn($set->sellingVatAccount->accountName, $cmp->translate('Selling VAT account'))->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
        $this->addColumn(app_TableModelViewColumn($set->unit->name, $cmp->translate('Unit'))->setVisible(true)->setSortable(true)->setExportable(true)->addClass('widget-column-thin'));
    }
    /**
     * @param Article  $record
     * @param string    $fieldPath
     * @return \Widget_Item
     */
    protected function computeCellContent(Article $record, $fieldPath)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        switch ($fieldPath) {
            case 'name':
            case 'reference':
                return $W->Link(
                    $record->$fieldPath,
                    $this->ctrl->display($record->id)
                );
                break;
            case 'visual':
                $path = $record->getPhotoPath();
                if($path == null){
                    return null;
                }
                return $W->ImageThumbnail($path, $record->name)->setThumbnailSize(40,40)->addClass('app-article-photo widget-align-center');
        }
        
        return parent::computeCellContent($record, $fieldPath);
    }
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param array       $filter
     * @return \ORM_Criteria
     */
    public function getFilterCriteria($filter = null)
    {
        /* @var $recordSet ArticleSet */
        $recordSet = $this->getRecordSet();
        $App = $this->App();
        $conditions = array(
            $recordSet->isReadable()
        );
        
        if (isset($filter['search']) && !empty($filter['search'])) {
            $mixedConditions = array(
                $recordSet->name->contains($filter['search']),
                $recordSet->reference->contains($filter['search'])
            );
            $conditions[] = $recordSet->any($mixedConditions);
        }
        
        $conditions[] = parent::getFilterCriteria($filter);
        
        return $recordSet->all($conditions);
    }
}