<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ui;

use Capwelton\App\Article\Set\ArticleFamily;

/**
 * @property $record ArticleFamily
 */
class ArticleFamilyLine extends \Widget_Widget
{
    private $App;
    protected $record;
    
    public function __construct(\Func_App $App, ArticleFamily $record, $id = null)
    {
        parent::__construct($id);
        $this->record = $record;
        $this->App = $App;
    }
    
    public function App()
    {
        return $this->App;
    }
    
    public function familyLine()
    {        
        $App = $this->App();
        $W = bab_Widgets();
        
        $box = $W->FlowItems()->setVerticalSpacing(1, 'em')->setIconFormat(16, 'left');
        
        $families = array_reverse($this->record->getAscendants());
        $families[$this->record->id] = $this->record;
        
        $isFirstElement = true;
        
        foreach ($families as $family){
            if(!$isFirstElement){
                $box->addItem(
                    $W->FlowItems(
                        $W->Label(bab_nbsp()),
                        $W->Icon('', \Func_Icons::ACTIONS_ARROW_RIGHT),
                        $W->Label(bab_nbsp())
                    )
                );
            }
            $isFirstElement = false;
            
            $box->addItem(
                $familyCell = $W->FlowItems(
                    $W->Icon('', \Func_Icons::ACTIONS_VIEW_LIST_TREE),
                    $W->Label(bab_nbsp()),
                    $W->Label($family->name)
                )->addClass('family-node')
            );
        }
        
        return $box;
    }
    
    public function display(\Widget_Canvas $canvas)
    {
        $output = parent::display($canvas);        
        $output .= $canvas->html($this->getId(), $this->getClasses(), $this->familyLine());
        
        return $output;
    }
}