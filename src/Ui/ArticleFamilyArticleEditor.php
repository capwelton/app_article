<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ui;

use Capwelton\App\Article\Set\Article;

class ArticleFamilyArticleEditor extends \app_Editor
{
    protected $record;
    
    public function __construct(\Func_App $app, Article $record, $id = null, $layout = null)
    {
        $this->record = $record;
        parent::__construct($app, $id, $layout);
    }
        
    protected function prependFields()
    {
        $this->treeview();
    }
    
    protected function treeview()
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $treeview = $W->SimpleTreeView();
        $treeview->addClass('article-family-treeview');
        $treeview->setPersistent(false);
        
        $set = $App->ArticleFamilySet();
        $nodes = $set->select(
            $set->parent->isNot(0)    
        );        
        
        $icon = $GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png';
        
        foreach ($nodes as $node) {
            $element = &$treeview->createElement($node->id, '', $node->name, '', '');
            $element->setIcon($icon);
            
            $selected = $node->id == $this->record->family;
            $element->addCheckBox('data[articleFamilies][' . $node->id . ']', $selected);
            $parentNode = $node->parent();
            
            $parentId = $parentNode && $parentNode->parent != 0 ? $parentNode->id : null;
            $treeview->appendElement($element, $parentId);
        }
        
        $this->addItem($treeview);
    }
    
    /**
     *
     * @param \Widget_Canvas $canvas
     */
    public function display(\Widget_Canvas $canvas)
    {
        $this->initialize();
        $displayable = parent::display($canvas);
        $displayable .= $canvas->loadScript($this->getId(), $this->App()->getComponentByName('Article')->getScriptPath().'articlefamilytree.js'.'?'.microtime(true));
        return $displayable;
    }
}