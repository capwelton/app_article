<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ui;

use Capwelton\App\Article\Ctrl\ArticleController;

/**
 * ArticleRecordView
 *
 * @method \Func_App    App()
 * @property Article $record
 */
class ArticleRecordView extends \app_RecordView
{
    /**
     * @var ArticleController
     */
    private $ctrlNoProxy;
    /**
     * @var ArticleController
     */
    private $ctrl;
    
    public function __construct(\Func_App $App, $id = null, \Widget_Layout $layout = null)
    {
        parent::__construct($App, $id, $layout);
        $this->ctrlNoProxy = $App->Controller()->Article(false);
        $this->ctrl = $this->ctrlNoProxy->proxy();
    }
    /**
     * {@inheritDoc}
     * @see \app_UiObject::display()
     */
    public function display(\Widget_Canvas $canvas)
    {
        $this->addSections($this->getView());
        
        return parent::display($canvas);
    }
    
    protected function _attachments(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $attachmentComponent = $App->getComponentByName('ATTACHMENT');
        if(!isset($attachmentComponent)){
            return null;
        }
        
        return $this->labelledWidget(
            isset($label) ? $label : $attachmentComponent->translate('Attachments'),
            $App->Controller()->Attachment(false)->_attachments($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _tags(\app_CustomSection $customSection = null, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $tagComponent = $App->getComponentByName("TAG");
        if(!isset($tagComponent)){
            return null;
        }
        
        $tagsDisplay = $App->Ui()->TagsListForRecord($this->record, $App->Controller()->Article()->displayListForTag());
        $editor = new \app_Editor($App);
        $editor->addItem($App->Controller()->Tag(false)->SuggestTag()->setName('label'));
        
        $editor->setName('tag');
        $editor->setSaveAction($App->Controller()->Tag()->link(),$tagComponent->translate("Add"));
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setHiddenValue('to', $this->record->getRef());
        $editor->isAjax = true;
        
        return $this->labelledWidget(
            $tagComponent->translate('Tags'),
            $W->FlowItems(
                $tagsDisplay,
                $editor
            )->setHorizontalSpacing(0.5, 'em'),
            $customSection
        );
    }
    
    protected function _description(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        return $this->labelledWidget(
            isset($label) ? $label : $App->translate('Description'),
            $W->Html($this->record->description),
            $customSection
        );
    }
    
    protected function _notes($customSection)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        return $this->labelledWidget(
            $App->translate('Notes'),
            $W->VBoxItems(
                $W->Link(
                    '',
                    $App->Controller()->Note()->add($this->record->getRef())
                )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
                ->setSizePolicy('pull-up')
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setTitle($App->translate('Add note')),
                $App->Controller()->Note(false)->listFor($this->record->getRef())
            )->setIconFormat(16, 'left'),
            $customSection
        );
    }
    
    protected function _teams($customSection)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        return $this->labelledWidget(
            $App->translate('Teams'),
            $W->VBoxItems(
                $W->Link(
                    '',
                    $App->Controller()->Team()->add($this->record->getRef())
                )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
                ->setSizePolicy('pull-up')
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setTitle($App->translate('Add team')),
                $App->Controller()->Team(false)->listFor($this->record->getRef())
            )->setIconFormat(16, 'left'),
            $customSection
        );
    }
    
    protected function _organization($customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $widget = $W->Label($App->translate('No organization found'));
        if($org = $this->record->organization()){
            $widget = $W->Link(
                $org->getFullName(),
                $App->Controller()->Organization()->display($this->record->organization)
            )->setOpenMode(\Widget_Link::OPEN_TAB)->setIcon(\Func_Icons_Awesome::WEBAPP_EXTERNAL_LINK);
        }
        else{
            $widget->setSizePolicy('alert alert-danger');
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Organization'),
            $widget,
            $customSection
        )->setIconFormat(16, 'left');
    }
    
    protected function _currency($customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $widget = $W->Label($App->translate('No organization found'));
        if($org = $this->record->organization()){
            $orgCurrency = '';
            if($currency = $org->currency()){
                $orgCurrency = sprintf('%s (%s)', $currency->name_fr, $currency->symbol);
            }
            $widget->setText($orgCurrency);
        }
        else{
            $widget->setSizePolicy('alert alert-danger');
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Currency'),
            $widget,
            $customSection
        )->setIconFormat(16, 'left');
    }
    
    protected function _rentingCurrency($customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $widget = $W->Label('');
        if($currency = $this->record->rentingCurrency()){
            $widget->setText(sprintf('%s (%s)', $currency->name_fr, $currency->symbol));
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Currency'),
            $widget,
            $customSection
        )->setIconFormat(16, 'left');
    }
    
    protected function _category($customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Category'),
            $W->Label($this->record->category()->name),
            $customSection
        );
    }
    
    protected function _unit($customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Unit'),
            $W->Label($this->record->unit()->name),
            $customSection
        );
    }
    
    protected function _stockingUnit($customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Stocking unit'),
            $W->Label($this->record->stockingUnit()->name),
            $customSection
        );
    }
    
    protected function _rentingUnit($customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Renting unit'),
            $W->Label($this->record->rentingUnit()->name),
            $customSection
        );
    }
    
    protected function _family($customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $family = $this->record->family();
        if(isset($family)){
            $familyLine = $App->Ui()->ArticleFamilyLine($family);
        }
        else{
            $familyLine = $W->Label($App->translate('This article does not have a family'));
        }
        
        $buttonsBox = $W->Link(
            $App->translate('Edit family'),
            $this->ctrl->editFamily($this->record->id, true)
        )->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
        ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD);
            
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Article family'),
            $W->FlowItems(
                $familyLine,
                $buttonsBox
            )->setSizePolicy('widget-list-element')
            ->addClass('widget-actions-target')
            ->setVerticalAlign('top'),
            $customSection
        )->setIconFormat(16, 'left');
    }
    
    protected function _mainPhoto($customSection, $label = null)
    {
        return $this->ctrlNoProxy->_photos($this->record->id, true, true);
    }
    
    protected function _gallery($customSection, $label = null)
    {
        return $this->ctrlNoProxy->_photos($this->record->id, true, false);
    }
    
    protected function _sellingVatAccount(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $sellingVatAccount = '';
        if($vatAccount = $this->record->sellingVatAccount()){
            $sellingVatAccount = $vatAccount->display();
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Selling VAT account'),
            $W->Label($sellingVatAccount),
            $customSection
        );
    }
    
    protected function _rentingVatAccount(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $rentingVatAccount = '';
        if($vatAccount = $this->record->rentingVatAccount()){
            $rentingVatAccount = $vatAccount->display();
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Renting VAT account'),
            $W->Label($rentingVatAccount),
            $customSection
        );
    }
    
    protected function _dimensions(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        return $this->LabelledWidget(
            !empty($label) ? $label : $App->translate('Dimensions'),
            $W->Label(
                sprintf('%scm x %scm x %scm', $this->record->width, $this->record->height, $this->record->length)
            ),
            $customSection
        );
    }
    
    protected function _weight(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        return $this->LabelledWidget(
            !empty($label) ? $label : $App->translate('Weight'),
            $W->Label(
                sprintf('%skg', $this->record->weight)
            ),
            $customSection
        );
    }
}
