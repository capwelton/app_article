<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ui;

use Capwelton\App\Article\Set\Article;
use Capwelton\App\VatAccount\Set\VatAccount;
use Capwelton\App\Currency\Set\Currency;

/**
 *
 * @method \Func_App App()
 */
class ArticleViewEditor extends ArticleSectionEditor
{
    protected $ctrlNoProxy;
    
    public function __construct(\Func_App $app, Article $record, $view = '', $id = null, $layout = null)
    {
        parent::__construct($app, $id, $layout);
        $this->setHiddenValue('tg', $app->controllerTg);
        $this->setName('data');
        $this->record = $record;
        $this->ctrlNoProxy = $app->Controller()->Article(false);
        $this->addSections($view);
        $this->setRecord($record);
    }
    
    /**
     * @return \Widget_Layout
     */
    protected function buttonsLayout()
    {
        $W = bab_Widgets();
        return $W->VBoxLayout()->setSizePolicy('widget-form-buttons');
    }
    
    protected function _mainPhoto($customSection, $label = null)
    {
        $W = bab_Widgets();
        return $this->ctrlNoProxy->_photos($this->record->id, true, true);
    }
    
    protected function _family($customSection, $label = null)
    {
        return $this->App()->Ui()->ArticleFamilyArticleEditor($this->record);
    }
}