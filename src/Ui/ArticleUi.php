<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ui;

use Capwelton\App\Article\Set\Article;

class ArticleUi extends \app_Ui implements \app_ComponentUi
{    
    /**
     * Article unit editor
     * @return ArticleEditor
     */
    public function ArticleEditor(Article $record = null)
    {
        return new ArticleEditor($this->app, $record);
    }
    
    /**
     * Article unit table view
     * @return ArticleTableView
     */
    public function ArticleTableView()
    {
        return new ArticleTableView($this->app);
    }
    
    public function tableView()
    {
        return $this->ArticleTableView();
    }
    
    public function editor(Article $record = null)
    {
        return $this->ArticleEditor($record);
    }
    
    /**
     * @param Article $article
     * @return ArticleRecordView
     */
    public function ArticleFullFrame(Article $article)
    {
        $ArticleFullFrame = new ArticleRecordView($this->app);
        $ArticleFullFrame->setRecord($article);
        return $ArticleFullFrame;
    }
    
    /**
     * Article section editor
     * @return ArticleSectionEditor
     */
    public function ArticleSectionEditor(Article $record = null)
    {
        return new ArticleSectionEditor($this->app, $record);
    }
    
    /**
     * @param Article $record
     * @return ArticleFamilyArticleEditor
     */
    public function ArticleFamilyArticleEditor(Article $record = null)
    {
        return new ArticleFamilyArticleEditor($this->app, $record);
    }
    
    /**
     * @param Article   $record
     * @param string    $view
     * @return ArticleViewEditor
     */
    public function ArticleViewEditor(Article $record, $view = '', $id = null, $layout = null)
    {
        return new ArticleViewEditor($this->app, $record, $view, $id, $layout);
    }
}
