<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ui;

use Capwelton\App\Article\Set\Article;
use Capwelton\App\Article\Set\ArticleCategory;

/**
 * Article type editor
 * @return ArticleEditor
 */
class ArticleEditor extends \app_Editor
{
    /**
     * @var Article
     */
    protected $record = null;
    
    
    /**
     *
     * @param \Func_App $App
     * @param Article $record
     * @param string $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, Article $record = null, $id = null, \Widget_Layout $layout = null)
    {
        $this->record = $record;
        
        parent::__construct($App, $id, $layout);
        $this->setName('data');
        
        $this->colon();
        
        $this->addFields();
        $this->addButtons();
        
        $this->setHiddenValue('tg', $App->controllerTg);
        
        if (isset($record)) {
            $this->setHiddenValue('data[id]', $record->id);
            $this->setValues($record, array('data'));
        }
    }
    
    protected function addFields()
    {
        $W = bab_Widgets();
        
        $this->addItem($this->name());
        $this->addItem($this->organization());
        $this->addItem($this->type());
        $this->addItem(
            $W->FlexItems(
                $this->reference(),
                $this->category()
            )->setGrowable()
        );
        $this->addItem($this->barcode());
        $this->addItem($this->description());
        $this->addItem(
            $W->HBoxItems(
                $this->canBeSold(),
                $this->canBePurchased()
            )->setHorizontalSpacing(1, 'em')
        );
    }
    
    protected function addButtons()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $this->addButton(
            $submit = $W->SubmitButton()
            ->setLabel($App->translate('Save'))
            ->validate(true)
        );
        
        $this->addButton(
            $cancel = $W->SubmitButton()
            ->setLabel($App->translate('Cancel'))
        );
        
        $ctrl = $App->Controller()->Article();
        
        if(bab_isAjaxRequest()){
            $submit->setAjaxAction($ctrl->save());
            $cancel->setAjaxAction($ctrl->cancel());
        }
        else{
            $submit->setAction($ctrl->save());
            $cancel->setAction($ctrl->cancel());
        }
    }
    
    /**
     * @return \Widget_Item
     */
    protected function name()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Name'),
            $W->LineEdit()
            ->setMandatory(true, $App->translate('The name is mandatory'))
            ->addClass('widget-100pc'),
            'name'
        );
    }
    
    protected function organization()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Organization'),
            $App->Ui()->SuggestOrganization(false)
            ->setMandatory(true, $App->translate('The organization is mandatory'))
            ->addClass('widget-100pc'),
            'organization'
        );
    }
    
    /**
     * @return \Widget_Item
     */
    protected function reference()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Reference'),
            $W->LineEdit()
            ->setMandatory(true, $App->translate('The reference is mandatory'))
            ->addClass('widget-100pc'),
            'reference'
        );
    }
    
    /**
     * @return \Widget_Item
     */
    protected function category()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $options = array();
        $set = $App->ArticleCategorySet();
        $records = $set->select()->orderAsc($set->name);
        foreach ($records as $record){
            $options[$record->id] = $record->name;
        }
        $select = $W->Select2()->setOptions($options);
        
        return $this->labelledField(
            $App->translate('Category'),
            $select
            ->setMandatory(true, $App->translate('The category is mandatory'))
            ->addClass('widget-100pc'),
            'category'
        );
    }
    
    /**
     * @return \Widget_Item
     */
    protected function barcode()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Barcode'),
            $W->LineEdit()
            ->addClass('widget-100pc'),
            'barcode'
        );
    }
    
    /**
     * @return \Widget_Item
     */
    protected function description()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Description'),
            $W->CKEditor()
            ->addClass('widget-100pc'),
            'description'
        );
    }
    
    /**
     * @return \Widget_Item
     */
    protected function canBeSold()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Can be sold'),
            $W->CheckBox()
            ->addClass('widget-100pc'),
            'canBeSold'
        );
    }
    
    /**
     * @return \Widget_Item
     */
    protected function canBePurchased()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            $App->translate('Can be purhased'),
            $W->CheckBox()
            ->addClass('widget-100pc'),
            'canBePurchased'
        );
    }
    
    public function setValues($record, $namePathBase = array())
    {
        if ($record instanceof Article) {
            $values = $record->getFormOutputValues();
            $this->setValues(array('data' => $values));
        } else {
            parent::setValues($record, $namePathBase);
        }
    }
}