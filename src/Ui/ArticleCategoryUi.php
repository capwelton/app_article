<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ui;

use Capwelton\App\Article\Set\ArticleCategory;

class ArticleCategoryUi extends \app_Ui implements \app_ComponentUi
{    
    /**
     * Article category editor
     * @return ArticleCategoryEditor
     */
    public function ArticleCategoryEditor(ArticleCategory $record = null)
    {
        return new ArticleCategoryEditor($this->app, $record);
    }
    
    /**
     * Article category table view
     * @return ArticleCategoryTableView
     */
    public function ArticleCategoryTableView()
    {
        return new ArticleCategoryTableView($this->app);
    }
    
    public function tableView()
    {
        return $this->ArticleCategoryTableView();
    }
    
    public function editor(ArticleCategory $record = null)
    {
        return $this->ArticleCategoryEditor($record);
    }
}
