<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ui;

use Capwelton\App\Article\Set\Article;
use Capwelton\App\VatAccount\Set\VatAccount;
use Capwelton\App\Currency\Set\Currency;

/**
 *
 * @method \Func_App App()
 */
class ArticleSectionEditor extends \app_RecordEditor
{
    
    protected $parentOrganizationItem = null;
    
    protected $parentOrganizationField = null;
    
    
    
    public function setRecord($record)
    {
        $this->record = $record;
        $this->recordSet = $record->getParentSet();
        
        if ($name = $this->getName()) {
            $values = $record->getFormOutputValues(); 
            $this->setValues($values, array($name));
        }
        
        return $this;
    }
    
    /**
     * {@inheritDoc}
     * @see \app_Editor::setValues()
     */
    public function setValues($article, $namePathBase = array())
    {
        $App = $this->App();
        
        if ($article instanceof Article) {
            $values = $article->getFormOutputValues();
            $this->setValues(array('data' => $values));
        } else {
            parent::setValues($article, $namePathBase);
        }
        return $this;
    }
    
    protected function _createdBy(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _createdOn(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _modifiedBy(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _modifiedOn(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _id(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _uuid(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _family(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _name(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        return $this->labelledField(
            !empty($label) ? $label : $App->translate('Name'),
            $W->LineEdit()->setMandatory(true, $App->translate('The name is mandatory'))->addClass('widget-100pc'),
            'name'
        );
    }
    
    protected function _reference(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        return $this->labelledField(
            !empty($label) ? $label : $App->translate('Reference'),
            $W->LineEdit()->setMandatory(true, $App->translate('The reference is mandatory'))->addClass('widget-100pc'),
            'reference'
        );
    }
    
    protected function _description(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        return $this->labelledField(
            !empty($label) ? $label : $App->translate('Description'),
            $this->descriptionItem = $W->CKEditor()
            ->addClass('widget-100pc'),
            'description'
        );
    }
    
    protected function _category(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $select = $W->Select2();
        $set = $App->ArticleCategorySet();
        
        $records = $set->select()->orderAsc($set->name);
        $options = array();
        foreach($records as $record){
            $options[$record->id] = $record->name;
        }
        $select->setOptions($options);
        
        return $this->labelledField(
            !empty($label) ? $label : $App->translate('Category'),
            $select->addClass('widget-100pc'),
            'category'
        );
    }
    
    protected function _unit(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $select = $W->Select2();
        $set = $App->ArticleUnitSet();
        
        $records = $set->select()->orderAsc($set->name);
        $options = array();
        foreach($records as $record){
            $options[$record->id] = $record->name;
        }
        $select->setOptions($options);
        
        return $this->labelledField(
            !empty($label) ? $label : $App->translate('Unit'),
            $select->addClass('widget-100pc'),
            'unit'
        );
    }
    
    protected function _stockingUnit(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $select = $W->Select2();
        $set = $App->ArticleUnitSet();
        
        $records = $set->select()->orderAsc($set->name);
        $options = array();
        foreach($records as $record){
            $options[$record->id] = $record->name;
        }
        $select->setOptions($options);
        
        return $this->labelledField(
            !empty($label) ? $label : $App->translate('Stocking unit'),
            $select->addClass('widget-100pc'),
            'stockingUnit'
        );
    }
    
    protected function _rentingUnit(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $select = $W->Select2();
        $set = $App->ArticleUnitSet();
        
        $records = $set->select()->orderAsc($set->name);
        $options = array();
        foreach($records as $record){
            $options[$record->id] = $record->name;
        }
        $select->setOptions($options);
        
        return $this->labelledField(
            !empty($label) ? $label : $App->translate('Renting unit'),
            $select->addClass('widget-100pc'),
            'rentingUnit'
        );
    }
    
    protected function _sellingVatAccount(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $select = $W->Select2();
        $set = $App->VatAccountSet();
        
        $records = $set->select()->orderAsc($set->isDefault)->orderAsc($set->accountName);
        $options = array();
        foreach($records as $record){
            /* @var $record VatAccount */
            $options[$record->id] = $record->display();
        }
        $select->setOptions($options);
        
        return $this->labelledField(
            !empty($label) ? $label : $App->translate('Selling VAT account'),
            $select->addClass('widget-100pc'),
            'sellingVatAccount'
        );
    }
    
    protected function _rentingVatAccount(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $select = $W->Select2();
        $set = $App->VatAccountSet();
        
        $records = $set->select()->orderAsc($set->isDefault)->orderAsc($set->accountName);
        $options = array();
        foreach($records as $record){
            /* @var $record VatAccount */
            $options[$record->id] = $record->display();
        }
        $select->setOptions($options);
        
        return $this->labelledField(
            !empty($label) ? $label : $App->translate('Renting VAT account'),
            $select->addClass('widget-100pc'),
            'rentingVatAccount'
        );
    }
    
    protected function _rentingCurrency(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $select = $W->Select2();
        $set = $App->CurrencySet();
        
        $records = $set->select()->orderAsc($set->isDefault)->orderAsc($set->name_fr);
        $options = array();
        foreach($records as $record){
            /* @var $record Currency */
            $options[$record->id] = sprintf('%s (%s)', $record->name_fr, $record->symbol);
        }
        $select->setOptions($options);
        
        return $this->labelledField(
            !empty($label) ? $label : $App->translate('Renting currency'),
            $select->addClass('widget-100pc'),
            'rentingCurrency'
        );
    }
    
    protected function _organization(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        $item = $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Organization'),
            $suggest = $Ui->SuggestOrganization()->setMandatory(true, $App->translate('The organization is mandatory')),
            $customSection
        );
        
        if($organization = $this->record->organization()){
            $suggest->addOption($organization->id, $organization->name);
            $suggest->setValue($organization->id);
        }
        else{
            $suggest->addOption(0, '');
            $suggest->setValue(0);
        }
        
        return $item;
    }
    
    protected function _dimensions(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $box = $W->FlexLayout()->setGrowable();
        
        $box->addItems(
            $W->LabelledWidget(
                $App->translate('Width'),
                $W->NumberEdit()->setMin(0)->setStep('0.01'),
                'width',
                null,
                'cm'
            ),  
            $W->LabelledWidget(
                $App->translate('Height'),
                $W->NumberEdit()->setMin(0)->setStep('0.01'),
                'height',
                null,
                'cm'
            ),   
            $W->LabelledWidget(
                $App->translate('Length'),
                $W->NumberEdit()->setMin(0)->setStep('0.01'),
                'length',
                null,
                'cm'
            )    
        );
        
        return $this->LabelledWidget(
            !empty($label) ? $label : $App->translate('Dimensions'),
            $box,
            $customSection
        );
    }
    
    protected function _weight(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = $this->widgets;
        
        return $this->labelledField(
            !empty($label) ? $label : $App->translate('Weight'),
            $W->NumberEdit()->setMin(0)->setStep('0.01'),
            'weight',
            null, 
            'kg'
        );
    }
}