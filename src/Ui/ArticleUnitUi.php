<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Article\Ui;

use Capwelton\App\Article\Set\ArticleUnit;

class ArticleUnitUi extends \app_Ui implements \app_ComponentUi
{    
    /**
     * Article unit editor
     * @return ArticleUnitEditor
     */
    public function ArticleUnitEditor(ArticleUnit $record = null)
    {
        return new ArticleUnitEditor($this->app, $record);
    }
    
    /**
     * Article unit table view
     * @return ArticleUnitTableView
     */
    public function ArticleUnitTableView()
    {
        return new ArticleUnitTableView($this->app);
    }
    
    public function tableView()
    {
        return $this->ArticleUnitTableView();
    }
    
    public function editor(ArticleUnit $record = null)
    {
        return $this->ArticleUnitEditor($record);
    }
}
