��    _                      	          $  
   (     3     <     J     S     [     n          �     �     �     �     �     �     �     �     �     �     	      	     >	     X	     a	     h	     q	  
   }	     �	     �	     �	     �	     �	     �	     �	     �	     
  !   	
     +
     2
     E
     T
     c
     o
     {
  -   �
     �
     �
  
   �
     �
     �
     �
     �
     �
               $     1     ?     G  	   P     Z     s     �     �     �     �     �     �     �     �                          >     C  #   I  !   m     �     �     �     �     �  #        6     S     X     k     t     {     �     �  %  �     �     �     �     �     �                -     5     L     _     q     �     �  
   �     �     �     �  
   �     �     �       )   -  $   W     |  	   �     �     �  
   �     �      �     �               2     E     N     V     _     w          �     �     �     �     �  3   �          (     1     B     I     M     ^     b     z     �  	   �     �     �  	   �     �     �     �     �          &     9      E     f     l     �     �     �     �     �     �     �  '   �  )     $   /     T  !   q  %   �     �     �  -   �     #     *     A     J     Q     W     _     &      .   _       
   G   8       0   	   @   E   V   ]   -       P       +   2       >          ^   Y   %           <   1          \      B   9       ?   [           '             H   /       ;             K   I              L      U   (   R          M   Z   6   O   )   "   N   !                           *   J             X       7   5             T   3           S       #   D             $            4       A   :      Q              =   W      C       F      ,        Account code Actions/Tasks Add Add family Add note Add subfamily Add team Article Article categories Article families Article family Article units Attached files Attachments Barcode Can be purhased Can be sold Cancel Category Completed tasks Confirm delete? Create a new article Create a new article category Create a new article unit Currency Delete Deletion Description Dimensions Edit article Edit article category Edit article family Edit article unit Edit family Edit view layout %s Families Family Gallery Generic description field in HTML Height Import view layout Is produceable Is purchasable Is rentable Is soldable Is stockable It will not be possible to undo this deletion Length Location Main photo Miscellaneous Name Name of the product No No organization found Nomenclature Notes Organization Pending tasks Product Quantity Reference Reference of the product Renting VAT account Renting account code Renting currency Renting unit Save Save the article family Selling Selling VAT account Selling warranty Service Stock Stocking unit Subtitle for the product name Tags Teams The article family has been deleted The article family has been saved The category is mandatory The name is mandatory The organization is mandatory The reference is mandatory The research is too short This article does not have a family This element does not exists Unit Unit selling price Variants Visual Weight Width Yes Project-Id-Version: apparticle
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-03-28 18:15+0200
Last-Translator: SI4YOU <contact@si-4you.com>
Language-Team: SI4YOU<contact@si-4you.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: libapp_translate;libapp_translate:1,2;translate:1,2;translate;translatable;translatable:1,2
X-Generator: Poedit 3.0.1
X-Poedit-SearchPath-0: .
 Code comptable Actions / Tâches Ajouter Ajouter une famille Ajouter une note Ajouter une sous-famille Ajouter une équipe Article Catégories d'articles Familles d'article Famille d'article Unités d'article Fichiers joints Fichiers joints Code-barre Peut être acheté Peut être vendu Annuler Catégorie Tâches completées Confirmer la suppression ? Créer un nouvel article Créer une nouvelle catégories d'article Créer une nouvelle unité d'article Devise Supprimer Suppression Description Dimensions Modifier l'article Modifier la catégorie d'article Modifier la famille d'article Modifier l'unité d'article Modifier la famille Modifier la vue %s Familles Famille Gallerie Description générique Hauteur Importer une vue Peut être produit Peut être acheté Peut être loué Peut être vendu Peut être stocké Il ne sera pas possible d'annuler cette suppression Longueur Location Photo principale Divers Nom Nom de l'article Non Aucun organisme trouvé Nomenclature Notes Organisme Tâches en attente Produit Quantité Référence Référence de l'article Compte TVA de location Code comptable de location Devise de location Unité de location Enregistrer Enregistrer la famille d'article Vente Compte TVA de vente Garantie de vente Service Stock Unité de stockage Sous-titre du nom de produit Tags Equipes La famille d'article a été supprimée La famille d'article a été enregistrée La catégorie doit être spécifiée Le nom doit être spécifié L'organisme doit être spécifié La référence doit être spécifiée La recherche est trop courte Cet article n'a pas de famille Cet élément n'existe pas ou est introuvable Unité Prix de vente unitaire Variants Visuel Poids Largeur Oui 