


jQuery(document).ready(function() {
    const config = { childList: true, subtree: true };
    const treeviewClass = 'article-family-treeview';

    const sleep = function(time) {
        return new Promise(resolve => setTimeout(resolve, time));
    }

    const onMutation = function(mutationsList) {
        let treeviews = $(`.${treeviewClass}:not(.${treeviewClass}-init)`);
        treeviews.each(function() {
            let treeview = $(this);
            treeview.addClass(`${treeviewClass}-init`);
            colorParentFamilies(treeview);
            expandSelectedFamilies(treeview);
            checkUncheckBoxes(treeview);
        });
    }

    const colorParentFamilies = function(treeview){
        treeview.find('.line:has(.level1)').addClass('treeview-sector');
        treeview.find('.line:has(.level2)').addClass('treeview-parent-family');
    }

    const checkUncheckBoxes = async function(treeview) {
        const checkboxes = treeview.find('input[type="checkbox"]');
        checkboxes.change(function(){
            if($(this).is(':checked')){
                checkboxes.prop('checked',false);
                $(this).prop('checked',true);
            }
        });
    }

    const expandSelectedFamilies = async function(treeview) {
        await sleep(100);
        const checkedElements = treeview.find('input[checked="checked"]');

        for (let element of checkedElements) {
            // loop through parents LI elements of checked inputs
            // to mark them as expanded
            while (!element.parentNode.classList.contains(treeviewClass)) {
                element = element.parentNode;
                if (element.tagName == "LI") {
                    // setAttribute('class'...) to overwrite previous class
                    element.setAttribute('class', 'bab_ul_tree_open');
                }
            }
        }
    }
    var observer = new MutationObserver(onMutation)
    observer.observe(document.body, config);
});
